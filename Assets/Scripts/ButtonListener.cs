﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonListener : MonoBehaviour {
	public Button startButton;
	public Button settingsButton;
	public Button quitButton;
	public Button multiButton;
	public Button localButton;
	public Button EggButton;
	public GameObject StartTypeContainer;
	public GameObject settingsMenu;
	bool startActive = false;
	bool settingActive = false;
	// Use this for initialization
	void Start () {
		Button startBtn = startButton.GetComponent<Button>();
		Button settingsBtn = settingsButton.GetComponent<Button>();
		Button quitBtn = quitButton.GetComponent<Button>();
		Button multiBtn = multiButton.GetComponent<Button>();
		Button localBtn = localButton.GetComponent<Button>();
		Button eggBtn = EggButton.GetComponent<Button>();
		startBtn.onClick.AddListener(startClicked);
		settingsBtn.onClick.AddListener(settingsClicked);
		quitBtn.onClick.AddListener(endGame);
		multiBtn.onClick.AddListener(loadMulti);
		localBtn.onClick.AddListener(loadLocal);
		eggBtn.onClick.AddListener(loadEgg);
		Settings.Start();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void startClicked() {
		if (startActive)
		{
			StartTypeContainer.SetActive(false);
			startActive = false;
		}
		else
		{
			StartTypeContainer.SetActive(true);
			startActive = true;			
		}
	}

	void settingsClicked() {
		if (settingActive)
		{
			settingsMenu.SetActive(false);
			startButton.gameObject.SetActive(true);
			quitButton.gameObject.SetActive(true);
			settingActive = false;
			PlayerPrefs.Save();
		}
		else
		{
			settingsMenu.SetActive(true);
			startButton.gameObject.SetActive(false);
			quitButton.gameObject.SetActive(false);
			settingActive = true;			
		}
	}

	void loadMulti() {
		Info.resetValues();
		Unit.resetValues();
		Info.isNetworked = true;
		InfoMulti.isNetworked = true;
		SceneManager.LoadScene(2);
	}

	void loadLocal() {
		Info.resetValues();
		Unit.resetValues();
		Button btn = startButton.GetComponent<Button>();
		SceneManager.LoadScene(1);
	}

	void loadEgg() {
		SceneManager.LoadScene(4);
	}

	void endGame() {
		Application.Quit();
	}
}
