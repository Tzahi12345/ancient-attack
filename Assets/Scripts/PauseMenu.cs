﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
	bool paused;
	bool settingActive = false;
	public GameObject menuContainer;
	GameObject Camera;
	public GameObject settingsMenu;
	public Button unPauseButton;
	public Button settingsButton;
	public Button quitButton;
	public Button quitToMainMenuButton;
	// Use this for initialization
	void Start () {
		menuContainer.SetActive(false);
		paused = false;
		Camera = GameObject.FindWithTag("MainCamera");
		Button unPauseBtn = unPauseButton.GetComponent<Button>();
		Button settingsBtn = settingsButton.GetComponent<Button>();
		if (Info.isNetworked == false)
		{
			Button quitToMainMenuBtn = quitToMainMenuButton.GetComponent<Button>();
			quitToMainMenuBtn.onClick.AddListener(quitToMainMenu);
		}
		Button quitBtn = quitButton.GetComponent<Button>();
		unPauseBtn.onClick.AddListener(unPause);
		settingsBtn.onClick.AddListener(settingsClicked);
		quitBtn.onClick.AddListener(endGame);
	}

	void Pause() {
		menuContainer.SetActive(true);
		paused = true;
		if (Info.isNetworked)
		Time.timeScale = 1.0f;
		else
		Time.timeScale = 0.0f;
	}

	void unPause() {
		menuContainer.SetActive(false);
		paused = false;
		Time.timeScale = 1.0f;				
	}

	void settingsClicked() {
		if (settingActive)
		{
			settingsMenu.SetActive(false);
			unPauseButton.gameObject.SetActive(true);
			quitButton.gameObject.SetActive(true);
			settingActive = false;
			PlayerPrefs.Save();
		}
		else
		{
			settingsMenu.SetActive(true);
			unPauseButton.gameObject.SetActive(false);
			quitButton.gameObject.SetActive(false);
			settingActive = true;			
		}
	}

	void quitToMainMenu() {
		unPause();
		SceneManager.LoadScene(0);
	}

	void endGame() {
		Application.Quit();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("escape")) {
			if (paused && !settingActive)
			{
				unPause();
			}
			else if (settingActive)
			{
				settingsClicked();
			}
			else
			{
				Pause();
			}
		}
	}
}
