﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : MonoBehaviour {
	public string projectile;
	public float rangeDistance = 120f;
	public float projectileInterval = 0.25f;
	public float attackInterval = 1f;
	float projectileTimer;
	float attackTimer;
	public int strengthHeavyArmor = 1;
	public int strengthLightArmor = 4;
	// Use this for initialization
	void Start () {
		projectileTimer = 0f;
		attackTimer = 0f;
	}

	void simulateProjectile() {
		BoxCollider2D collider = gameObject.GetComponent<BoxCollider2D>();
		float top = collider.offset.y + (collider.size.y / 2f);
		float btm = collider.offset.y - (collider.size.y / 2f);
		float left = collider.offset.x - (collider.size.x / 2f);
		float right = collider.offset.x + (collider.size.x /2f);
		
		Vector3 topLeft = transform.TransformPoint (new Vector3( left, top, 0f));
		Vector3 topRight = transform.TransformPoint (new Vector3( right, top, 0f));
		Vector3 btmLeft = transform.TransformPoint (new Vector3( left, btm, 0f));
		//Vector3 btmRight = transform.TransformPoint (new Vector3( right, btm, 0f));

		float width = Vector3.Distance(topLeft, topRight);
		float height = Vector3.Distance(topLeft, btmLeft);

		float posWidth = Random.Range(-0.5f,0.5f)*width;
		float posHeight = Random.Range(-0.5f,0.5f)*height;

		Vector3 pos = new Vector3(transform.position.x+posWidth, transform.position.y+posHeight, 0f);

		GameObject arrow = GameObject.Instantiate(Resources.Load<GameObject>("Projectiles/"+projectile),pos,transform.rotation);
		Rigidbody2D rigidbody = arrow.GetComponent<Rigidbody2D>();
		rigidbody.velocity = arrow.transform.up * 50;

		Vector2 dir = GetComponent<Unit>().engagedTo.transform.position - arrow.transform.position;
		float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 45f;
		arrow.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

		float distance = Vector2.Distance(arrow.transform.position, GetComponent<Unit>().engagedTo.transform.position);
		float time = distance/rigidbody.velocity.magnitude;

		Destroy(arrow, time);
	}

	void attack() {
		GameObject targetObj = GetComponent<Unit>().engagedTo;
		PlayerHealth healthScript = targetObj.GetComponent<PlayerHealth>();
		if (targetObj.GetComponent<Unit>().armorType == "heavy")
		{
			healthScript.AddjustCurrentHealth(strengthHeavyArmor * -1);
		}
		else
		{
			healthScript.AddjustCurrentHealth(strengthLightArmor * -1);
		}
	}
	
	// Update is called once per frame
	void Update () {
		projectileTimer += Time.deltaTime;
		attackTimer += Time.deltaTime;
		if (GetComponent<Attack>().isAttacking)
		{
			GetComponent<Unit>().disengageUnit();
		}
		if (GetComponent<Unit>().isEngaging)
		{
			if (Vector2.Distance(transform.position,GetComponent<Unit>().engagedTo.transform.position) > rangeDistance)
			{
				GetComponent<Unit>().disengageUnit();
			}
			if (projectileTimer > projectileInterval)
			{
				simulateProjectile();
				projectileTimer = 0f;
			}

			if (attackTimer > attackInterval)
			{
				attack();
				attackTimer = 0f;
			}
		}
	}
}
