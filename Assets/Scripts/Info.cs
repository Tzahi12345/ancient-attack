﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public static class Info {

	private static List<GameObject> enemies = new List<GameObject>();
	public static int teamID;
	public static bool isNetworked = false;
	public static GameObject clientObject;

	static List<GameObject> toList(GameObject[] inputObjects) {
		List<GameObject> objectsTemp = new List<GameObject>(); // instantiates list of combatants
		for (int i = 0; i < inputObjects.Length; i++) // Converts the array into a list
		{
			objectsTemp.Add(inputObjects[i]);
		}
		return objectsTemp;
	}

	/// DEPRECATED: 
	/// Gets list of enemies
	public static List<GameObject> getEnemies() {
		// DEPRECATED METHOD
		GameObject[] comradesArr = GameObject.FindGameObjectsWithTag("enemy");
		enemies = toList(comradesArr); // gets list of comrades
		return enemies;
	}

	public static List<GameObject> getEngagedUnits() { // returns list of engaged units
		List<GameObject> engagedEnemies = new List<GameObject>();
		List<GameObject> currentUnits = Unit.allEnemies; // right now only gets units that the enemy is engaged to
		Unit tempScript;
		foreach (GameObject unit in currentUnits)
		{
			tempScript = unit.GetComponent<Unit>();
			engagedEnemies.Add(tempScript.engagedTo);
		}
		return engagedEnemies;
	}

	public static List<GameObject> getFriendlies() {
		GameObject[] friendliesArrTemp = GameObject.FindGameObjectsWithTag("regular");
		GameObject addon = GameObject.FindGameObjectWithTag("select");
		List<GameObject> friendliesTemp = toList(friendliesArrTemp); // instantiates list of friendlies
		if (addon != null)
		{
			friendliesTemp.Add(addon);
		}
		return friendliesTemp;
	}
	/// Resets all the values in Info for script reuse
	public static void resetValues() {
		isNetworked = false;
	}
}
