﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EggHandler : MonoBehaviour {
	public InputField input;
	public AudioClip bustaClip;
	private AudioClip oldAudio;
	GameObject audioPersist;
	AudioSource mainAudio;
	AudioSource sideAudio;
	float timer;
	float audioTime;
	bool isPlaying;
	AudioClip theClip;
	// Use this for initialization
	void Start () {
		timer = 0;
		audioPersist = GameObject.Find("AudioPersist");
		mainAudio = audioPersist.GetComponent<AudioSource>();
		sideAudio = GetComponent<AudioSource>();
		sideAudio.volume = mainAudio.volume;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (input.text != "" && Input.GetKey(KeyCode.Return)) {
			switch (input.text)
			{
				case "nut":
					isPlaying = true;
					theClip = bustaClip;
					audioTime = theClip.length;
					timer = 0;
					sideAudio.clip = theClip;
					sideAudio.Play();
					mainAudio.Pause();
					break;
				default:
					break;
			}
			input.text = "";
     	}
		if (isPlaying) 
		{
			if (timer > audioTime)
			{
				mainAudio.Play();
				isPlaying = false;
				timer = 0;
			}
		}
	}
}
