﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

	public int Strength;
	float collisionTime;
	PlayerHealth colScript;
	private float topAngle;
    private float sideAngle; 
	public bool isAttacking;
	string colTag;
	public GameObject collider;
	// Use this for initialization
	void Start () {
		Vector2 size = GetComponent<BoxCollider2D>().size;
		size = Vector2.Scale (size, (Vector2)transform.localScale);
		topAngle = Mathf.Atan (size.x / size.y) * Mathf.Rad2Deg;
		sideAngle = 90.0f - topAngle;
	}
	
	// Update is called once per frame
	void Update () {
		collisionTime += Time.deltaTime;
		if (isAttacking)
		{
			if (collider == null || collider.Equals(null))
			{
				disableAttack();
			}
		}
		if (isAttacking)
		{
			float theDistance = Vector2.Distance(transform.position, collider.transform.position);
			if (theDistance > 20f)
			{
				isAttacking = false;
				collider = null;
			}
		}
		if (isAttacking)
		{
			if (collisionTime > 1)
			{
				collisionTime = 0;
				if (isFriendly(colTag) && Settings.friendlyFire == false)
				{

				}
				else
				{
					if (colTag == "enemy" || colTag == "select" || colTag == "regular")
					{
						colScript = collider.gameObject.GetComponent<PlayerHealth>();
						colScript.AddjustCurrentHealth(Strength * -1); // Reduces enemy health by Strength
					}
				}
			}
		}
	}

	bool isFriendly(string colliderTag)
	{
		if (colliderTag == "enemy")
		{
			if (transform.tag == colliderTag)
			return true;
			else
			return false;
		}
		else
		{
			if (transform.tag == "enemy")
			return false;
			else
			return true;
		}
	}

	void disableAttack() {
		isAttacking = false;
		collider = null;
	}

	void OnCollisionStay2D(Collision2D col)
	{		
		Vector3 v = (Vector3)col.contacts[0].point - transform.position;
 
        if (Vector3.Angle(v, transform.up) <= topAngle) { // top collision
			colTag = col.gameObject.tag;
			isAttacking = true;
			collider = col.gameObject;
        }
        else if (Vector3.Angle(v, transform.right) <= sideAngle)  { // right collision
			if (col.gameObject == collider)
			{
				disableAttack();
			}
        }
        else if (Vector3.Angle(v, -transform.right) <= sideAngle) { // left collision
			if (col.gameObject == collider)
			{
				disableAttack();
			}
        }
        else { // bottom collision
			if (col.gameObject == collider)
			{
				disableAttack();
			}
        }
	}
}
