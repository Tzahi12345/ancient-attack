﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class AIMovement : MonoBehaviour {

	public GameObject[] combatantsArr;
	public bool debugging = false;
	public List<GameObject> combatants;
	public List<float> distances;
	public bool isMoving;
	public float speedFactor = 1f;
	public Unit enemyUnitScript;
	public Unit unitScript;
	public float AIRange = 120f;
	public float rangeBuffer = 10f; //Buffer for range, if combatant goes AIRange+rangeBuffer distance out, will disengage
	public List<int> amounts;
	public int[] indexes;
	float randReload; // offset in time for finding out info so that all AI doesn't line up
	float reloadTime;
	public float AIRefreshTime = 2f;
	float distanceToEngage;

	GameObject toEngage;
	List<GameObject> toList(GameObject[] inputObjects) {
		List<GameObject> objectsTemp = new List<GameObject>(); // instantiates list of combatants
		for (int i = 0; i < inputObjects.Length; i++) // Converts the array into a list
		{
			objectsTemp.Add(inputObjects[i]);
		}
		return objectsTemp;
	}

	List<GameObject> getCombatants() {
		//LEGACY CODE:
		/*GameObject[] combatantsArrTemp = GameObject.FindGameObjectsWithTag("regular");
		GameObject addon = GameObject.FindGameObjectWithTag("select");
		List<GameObject> combatantsTemp = toList(combatantsArrTemp); // instantiates list of combatants
		if (addon != null)
		{
			combatantsTemp.Add(addon);
		}*/
		return Unit.allFriendlies;
	}

	List<GameObject> getComrades() {
		return Unit.allEnemies;
	}

	float getCombatDistance(GameObject target) {
		float distance;
		distance = Vector3.Distance(transform.position,target.transform.position);
		return distance;
	}

	List<float> getCombatDistances() {
		List<float> distancesTemp = new List<float>();
		for (int i = 0; i < Unit.allFriendlies.Count; i++)
		{
			float distance;
			distance = getCombatDistance(Unit.allFriendlies[i]);
			distancesTemp.Add(distance);
		}
		return distancesTemp;
	}

	List<GameObject> getEngagedEnemies() { // returns list of engaged enemies
		List<GameObject> engagedEnemies = new List<GameObject>();
		List<GameObject> currentComrades = getComrades();
		Unit tempScript;
		foreach (GameObject comrade in currentComrades)
		{
			tempScript = comrade.GetComponent<Unit>();
			engagedEnemies.Add(tempScript.engagedTo);
		}
		return engagedEnemies;
	}

	int amountEngagedBy(GameObject target) {
		int amount = 0;
		List<GameObject> currentComrades = getComrades();
		Unit tempScript;
		foreach (GameObject comrade in currentComrades)
		{
			tempScript = comrade.GetComponent<Unit>();
			if (tempScript.engagedTo == target)
			{
				amount++;
			}
		}
		return amount;
	}

	int amountEngagedBy2(GameObject target)
	{
		Unit tempUnitScript = target.GetComponent<Unit>();
		return tempUnitScript.amountEngaging;
	}

	/* Returns false when there are no combatants in range */
	bool anyCombatantsInRange() {
		
		return indexes.Length > 0;
	}

	GameObject getUnitToEngage() {
		bool alreadyEngaged = unitScript.isEngaging;
		
		if (indexes.Length > 0)
		{
			amounts = new List<int>();	
			for (int i = 0; i < indexes.Length; i++)
			{
				int tempAmount = amountEngagedBy(Unit.allFriendlies[indexes[i]]);
				amounts.Add(tempAmount);
			}

			if (Settings.maxEngage > 0)
			{
				float[] amountsArr = Array.ConvertAll(amounts.ToArray(), x => (float)x);
				int[] amountsIndexes = getIndexesLessThan(amountsArr, Settings.maxEngage - 1); //subtracts one because it engages too many due to timing
				if (amountsIndexes.Length == 0)
				{
					if (alreadyEngaged)
					{
						return unitScript.engagedTo;
					}
					else
					{
						return null;
					}
				}
				else
				{
					List<int> newAmounts = new List<int>();
					for (int i = 0; i < amountsIndexes.Length; i++)
					{
						newAmounts.Add(amounts[amountsIndexes[i]]);
					}
					amounts = newAmounts;
				}
			}

			int minIndex = amounts.IndexOf(amounts.Min());
			int minimum = amounts[minIndex];

			if (alreadyEngaged)
			{
				if (amountEngagedBy(unitScript.engagedTo) - 1 > minimum)
				{
					return Unit.allFriendlies[indexes[minIndex]];
				}
				else
				{
					return unitScript.engagedTo;
				}
			}
			else
			{
				return Unit.allFriendlies[indexes[minIndex]];
			}
		}
		else
		{
			if (alreadyEngaged)
			{
				return unitScript.engagedTo;
			}
			else
			{
				return null;
			}
		}
	}

	/** Returns true if target is found in enemies, essentially will say if target GameObject is
	* engaged by your comrades
	*/
	bool isEngaged(List<GameObject> enemies, GameObject target) {
		bool tempbool = false;
		for (int i = 0; i < enemies.Count; i++)
		{
			GameObject enemy = enemies[i];
			if (enemy == target)
			{
				tempbool = true;
			}
		}
		return tempbool;
	}

	/** Gets indexes of where something is less than value */
	int[] getIndexesLessThan(float[] targetArr, float value) {
		List<int> indexesInvolved = new List<int>();
		var resultArray = targetArr.Where((item, index) =>
			{ 
				if (item <= value) {
					indexesInvolved.Add(index);
					return true;
				} 
				else {
					return false;
				} 
			}
		).ToArray();
		var foundIndexArray = indexesInvolved.ToArray();
		return foundIndexArray;
	}

	bool mismatch() { // Returns true if there's a logic mismatch in some of the variables
		bool missingOrNull = unitScript.engagedTo == null || unitScript.engagedTo.Equals(null);
		bool mismatch1 = unitScript.isEngaging && missingOrNull; // if its engaging and its missing or null
		bool mismatch2 = unitScript.engagedTo != null && unitScript.engagedTo.Equals(null); // if its missing
		bool mismatch3 = unitScript.isEngaging == false && unitScript.engagedTo != null; // two variables should follow each other
		bool mismatch4 = missingOrNull && isMoving; // if its not engaged, shouldn't move
		return mismatch1 || mismatch2 || mismatch3 || mismatch4;
	}

	public void engage(GameObject combatantUnit) {
		Debug.Log("Engaged");
		unitScript.isEngaging = true;
		unitScript.engagedTo = combatantUnit;
		isMoving = true;
	}

	public void disengage() {
		Debug.Log("Disengaged");
		unitScript.isEngaging = false;
		unitScript.engagedTo = null;
		isMoving = false;
	}

	void refreshValues() {
		distances = getCombatDistances();
		unitScript = transform.gameObject.GetComponent<Unit>();
		indexes = getIndexesLessThan(distances.ToArray(), AIRange);
		if (unitScript.isEngaging)
		distanceToEngage = Vector3.Distance(transform.position,unitScript.engagedTo.transform.position);
	}

	// Use this for initialization
	void Start() {
		randReload = UnityEngine.Random.value * 2f;
		reloadTime = randReload;
		refreshValues();
	}
	
	// Update is called once per frame
	void Update() {
		reloadTime += Time.deltaTime;

		if (reloadTime > AIRefreshTime)
		{
			refreshValues();
			reloadTime = 0;
		}

		if (anyCombatantsInRange())
		{
			toEngage = getUnitToEngage();	
			if (toEngage != null && toEngage != unitScript.engagedTo) // Remove second half later to allow for multiple engagements
			{
				engage(toEngage);
			}		
		}

		if (mismatch())
		{
			disengage();
		}


		if (isMoving)
		{
			bool outOfRange = unitScript.engagedTo == null || unitScript.engagedTo.Equals(null) || getCombatDistance(unitScript.engagedTo) > AIRange+rangeBuffer;
			GameObject targetObject = unitScript.engagedTo;
			Vector2 dir = targetObject.transform.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			Vector2 target = new Vector2(targetObject.transform.position.x,
			                             targetObject.transform.position.y);
			bool tempStop = Vector2.Distance (transform.position, target) < 0.1f;
			if (!tempStop) { // if it reaches its position, it stops moving	
				transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 5 * Time.deltaTime * speedFactor);
			}
			if (outOfRange) {
				Debug.Log("Out of range ran!");
				disengage();
			}
		}
		
	}

	void OnCollisionEnter2D (Collision2D col)
    {
		if (col.gameObject.transform.tag != "enemy") //if it collides against something that doesn't have the enemy tag, it stops
		{
			isMoving = false;
		}
	}

	void OnCollisionExit2D ()
	{
		isMoving = true;
	}
}
