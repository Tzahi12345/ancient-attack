﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Unit : MonoBehaviour {
	public static List<GameObject> allEnemies = new List<GameObject>();
	public static List<GameObject> allFriendlies = new List<GameObject>();
	public bool isEngaging;
	public GameObject engagedTo;
	public string team;
	public int amountEngaging;
	public int teamID;
	public GameObject clientObject;
	public GameObject serverObject;
	public bool selfSpawned = false;
	public bool isRanged = false;
	[Tooltip("Armor Type: can be 'heavy' or 'light'")]
	public string armorType;

	public static void resetValues() {
		allEnemies = new List<GameObject>();
		allFriendlies = new List<GameObject>();
	}

	// Use this for initialization
	void Start () {
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < objects.Length; i++)
        {
            GameObject cur = objects[i];
            if (cur.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                clientObject = cur;
            }
        }
		amountEngaging = 0;
		isEngaging = false;
		if (Info.isNetworked)
		{
			if (GetComponent<NetworkIdentity>().hasAuthority)
			{
				Unit.allFriendlies.Add(gameObject);
				team = "friendly";
			}
			else
			{
				Unit.allEnemies.Add(gameObject);
				team = "enemy";
			}
		}
		else
		{
			if (transform.tag == "enemy")
			{
				Unit.allEnemies.Add(gameObject);
				team = "enemy";
			}
			else
			{
				Unit.allFriendlies.Add(gameObject);
				team = "friendly";
			}
		}
	}

	public void engageUnit(GameObject unitToEngage) {
		isEngaging = true;
		engagedTo = unitToEngage;
	}

	public void disengageUnit() {
		isEngaging = false;
		engagedTo = null;
	}
	
	// Update is called once per frame
	void Update () {
		if (Info.isNetworked && InfoMulti.isServer)
		{
			List<NetworkInstanceId> ids = new List<NetworkInstanceId>();
			List<Quaternion> rotations = new List<Quaternion>();
			List<GameObject> friendlies = Unit.allFriendlies;
			for (int i = 0; i < friendlies.Count; i++)
			{
				ids.Add(friendlies[i].GetComponent<NetworkIdentity>().netId);
				rotations.Add(friendlies[i].transform.rotation);
			}
			//clientObject.GetComponent<PlayerNetwork>().RpcUpdateRotations(ids.ToArray(), rotations.ToArray());
		}
	}

	

	void OnDestroy() {
		if (team == "friendly")
		{
			allFriendlies.Remove(gameObject);
		}
		else
		{
			allEnemies.Remove(gameObject);
		}
		if (Info.isNetworked)
		{
			if (team == "enemy")
			{
				List<GameObject> friendlies = allFriendlies;
				for (int i = 0; i < friendlies.Count; i++)
				{
					GameObject currentFriendly = friendlies[i];
					Unit tempScript = currentFriendly.GetComponent<Unit>();
					if (tempScript.isEngaging && tempScript.engagedTo == gameObject)
					{
						tempScript.disengageUnit();
					}
				}
			}
			else
			{
				List<GameObject> enemies = allEnemies;
				for (int i = 0; i < enemies.Count; i++)
				{
					GameObject currentEnemy = enemies[i];
					Unit tempScript = currentEnemy.GetComponent<Unit>();
					if (tempScript.isEngaging && tempScript.engagedTo == gameObject)
					{
						tempScript.disengageUnit();
					}
				}
			}
			SelectScriptMulti ssmTemp = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SelectScriptMulti>();
			ssmTemp.disableOutline(engagedTo);
		}
		else
		{
			if (team == "enemy")
			{
				List<GameObject> friendlies = allFriendlies;
				for (int i = 0; i < friendlies.Count; i++)
				{
					GameObject currentFriendly = friendlies[i];
					Unit unitScript = currentFriendly.GetComponent<Unit>();
					if (unitScript.isEngaging && unitScript.engagedTo == gameObject)
					{
						unitScript.disengageUnit();
					}
				}
			}
			else
			{
				List<GameObject> enemies = allEnemies;
				for (int i = 0; i < enemies.Count; i++)
				{
					GameObject currentEnemy = enemies[i];
					AIMovement tempScript = currentEnemy.GetComponent<AIMovement>();
					if (tempScript.unitScript.isEngaging && tempScript.unitScript.engagedTo == gameObject)
					{
						tempScript.disengage();
					}
				}
			}
		}
	}
}
