﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NotificationHandler : MonoBehaviour {

	public GameObject notificationObject;
	Text notificationText;
	string text;
	float timer;
	float interval;
	bool isShow;

	// Use this for initialization
	void Start () {
		isShow = false;
		timer = 0;
		notificationText = notificationObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isShow)
		{
			timer += Time.deltaTime;
			if (timer > interval)
			{
				isShow = false;
				notificationObject.SetActive(false);
			}
		}
		
	}

	public void displayText(string message, float time)
	{
		timer = 0f;
		notificationText.text = message;
		interval = time;
		notificationObject.SetActive(true);
		isShow = true;
	}
}
