﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class StartStopHandler : MonoBehaviour {

	public Button startStopButton;
	public Text startStopButtonText;
	
	// Use this for initialization
	void Start () {
		Button startStopBtn = startStopButton.GetComponent<Button>();
		startStopButtonText = startStopButton.GetComponentInChildren<Text>();
		startStopBtn.onClick.AddListener(startStopClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateText() {
		if (InfoMulti.isServer)
		{
			startStopButtonText.text = "Start";
		}
	}

	void startStopClicked()
	{
		if (InfoMulti.isServer)
		{
			GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
			SelectScriptMulti selectScript = camera.GetComponent<SelectScriptMulti>();
			selectScript.enableSelectOnly();
			GameObject clientObject;
			GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
			for (int i = 0; i < objects.Length; i++)
			{
				GameObject cur = objects[i];
				if (cur.GetComponent<NetworkIdentity>().isLocalPlayer)
				{
					clientObject = cur;
					clientObject.GetComponent<PlayerNetwork>().RpcEnableSelectOnlyMode();
				}
			}
			//NotificationHandler notification = camera.GetComponent<NotificationHandler>();
			//notification.displayText("Attack!", 2f);
			//startStopButton.gameObject.SetActive(false);
		}
		else
		{

		}
	}
}
