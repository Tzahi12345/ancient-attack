using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using cakeslice;

public class SelectScript : MonoBehaviour {
	public bool isMoving;
	public Vector2 origin;
	public Vector3 origin3;
	public GameObject selectObj;
	public Transform select;
	string regularTag = "regular";
	string selectedTag = "select";
	public bool selectedExists;

	public void Deselect() {
		//stopUnit();
		Transform select = GameObject.FindWithTag("select").transform;
		SpriteRenderer renderer = GameObject.FindWithTag("select").GetComponent<SpriteRenderer>();
		renderer.color = new Color(1f, 1f, 1f, 1f);
		select.tag = regularTag;
		isMoving = false;
		// makes sure outline is disabled for all enemy units
		List<GameObject> enemies = Info.getEnemies();
		for (int i = 0; i < enemies.Count; i++)
		{
			disableOutline(enemies[i]);
		}
	}

	void Select(RaycastHit2D hit) {
		GameObject obj = hit.transform.gameObject;
		if (isAssisted(obj))
		{
			Unit unitScript = obj.GetComponent<Unit>();
			enableOutline(unitScript.engagedTo);
		}
		hit.transform.gameObject.tag = "select";
		SpriteRenderer renderer = GameObject.FindWithTag("select").GetComponent<SpriteRenderer>();
		renderer.color = new Color(0f, 1f, 0f, 1f);
	}

	void moveUnit() {
		//Debug.Log ("Moving");
		select = GameObject.FindWithTag("select").transform;
		selectObj = GameObject.FindWithTag("select");
		Vector2 aPosition1 = origin;
		//Debug.Log (origin.ToString ());
		Movement moveScript = selectObj.GetComponent<Movement>();
		if (isAssisted(selectObj))
		{
			Unit unitScript = selectObj.GetComponent<Unit>();
			disableOutline(unitScript.engagedTo);
			unitScript.disengageUnit();
		}
		moveScript.isMoving = true;
		moveScript.target = origin;
		moveScript.target3 = origin3;
	}

	void disableOutline(GameObject obj)
	{
		Outline outline = obj.GetComponent<Outline>();
		outline.enabled = false;
	}

	void enableOutline(GameObject obj)
	{
		Outline outline = obj.GetComponent<Outline>();
		outline.enabled = true;
	}

	void moveGuided(GameObject enemy) {
		selectObj = GameObject.FindWithTag("select");
		select = selectObj.transform;
		Unit unitScript = selectObj.GetComponent<Unit>();
		if (isAssisted(selectObj)) // disables old outline if exists
		{
			disableOutline(unitScript.engagedTo);
		}
		unitScript.engageUnit(enemy);
		enableOutline(enemy);
		Movement moveScript = selectObj.GetComponent<Movement>();
		moveScript.isMoving = true;
	}

	void rangeAttack(GameObject enemy) {
		selectObj = GameObject.FindWithTag("select");
		select = selectObj.transform;	
		selectObj.GetComponent<Unit>().engageUnit(enemy);
		enableOutline(enemy);
		Movement moveScript = selectObj.GetComponent<Movement>();
		moveScript.isMoving = false;
	}

	bool isAssisted(GameObject obj) {
		Unit unitScript = obj.GetComponent<Unit>();
		if (unitScript.isEngaging)
		return true;
		else
		return false;
	}

	void stopUnit() {
		selectObj = GameObject.FindWithTag("select");
		Movement moveScript = selectObj.GetComponent<Movement>();
		moveScript.isMoving = false;
	}

	// Use this for initialization
	void Start () {
		isMoving = false;
	}

	// Update is called once per frame
	void Update () {
		selectedExists = (GameObject.FindWithTag("select") != null);
		if (Input.GetMouseButtonDown(0))
		{
			if (selectedExists) {
			}

			var layerMask = 1 << 2;

			layerMask = ~layerMask;

			origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
			                             Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
			origin3 = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
			                      Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
			                      Camera.main.ScreenToWorldPoint(Input.mousePosition).z);
			RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero, 0f, layerMask);
			if (hit && hit.transform.gameObject.tag != "enemy") { //if it hits and the hit is not an enemy or if its a tag
				string hitTag = hit.transform.gameObject.tag;
				print(hitTag);
				if (selectedExists)
				{
					Deselect();
					if (hitTag == regularTag)
					{
						Select(hit);
					}
				}
				else
				{
					if (hitTag == regularTag)
					{
						Select(hit);
					}
				}
			}
			else if (hit && hit.transform.gameObject.tag == "enemy" && Settings.assistMode) // what happens if its selected
			{
				selectObj = GameObject.FindWithTag("select");
				select = selectObj.transform;		
				if (selectedExists && selectObj.GetComponent<Unit>().isRanged == false)
				{
					moveGuided(hit.transform.gameObject);
				}
				else if (selectedExists && selectObj.GetComponent<Unit>().isRanged == true)
				{
					if (Vector2.Distance(select.position, hit.transform.position) <= selectObj.GetComponent<RangedAttack>().rangeDistance)
					{
						rangeAttack(hit.transform.gameObject);
					}
				}
			}
			else
			{
				if (selectedExists)
				{
					moveUnit();
				}
			}
		}
	}
}
