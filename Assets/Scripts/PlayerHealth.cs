﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerHealth : NetworkBehaviour {
    public int maxHealth = 100;
   
    public float healthBarLength;
    public float redBarLength;
    public float opacity;
    public int lengthFactor = 25;
    public int barOffsetX = -15;
    public float healthUpdateInterval = 0.3f;
    float healthUpdateTime;
    GameObject slider;
    Slider sliderSlide;
    GameObject canvas;
    public int personalCurHealth = 100;
    [SyncVar]
    public int curHealth = 100;
    GameObject clientObject;
   
    // Use this for initialization
    void Start () {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < objects.Length; i++)
        {
            GameObject cur = objects[i];
            if (cur.GetComponent<NetworkIdentity>().isLocalPlayer)
            {
                clientObject = cur;
            }
        }
        canvas = GameObject.Find("Canvas");
        healthBarLength = lengthFactor;
        redBarLength = 0;
        opacity = 0.5f;
        healthUpdateTime = 0 + Random.value;
        if (Network.isClient)
        {
            slider = Instantiate(Resources.Load<GameObject>("Slider"),transform.position,Quaternion.identity);
            
        }
        else
        {
            slider = Instantiate(Resources.Load<GameObject>("Slider"),transform.position,Quaternion.identity);
            
        }
        slider.transform.SetParent(canvas.transform);
        sliderSlide = slider.GetComponent<Slider>(); 
    }
   
    // Update is called once per frame
    void Update () {
        Vector2 targetPos = Camera.main.WorldToScreenPoint (transform.position);
        healthUpdateTime += Time.deltaTime;
        if (healthUpdateTime > healthUpdateInterval)
        {
            AddjustCurrentHealth(0);
            healthUpdateTime = 0;
        }
        slider.transform.position = targetPos;

    }
   
    public void AddjustCurrentHealth(int adj) {
        curHealth += adj;
        personalCurHealth += adj;

        if (curHealth <= 0)
        {
            Destroy(gameObject);
        }

        if (curHealth > maxHealth)
            curHealth = maxHealth;

        if(maxHealth < 1)
            maxHealth = 1;

        sliderSlide.value = curHealth;
        if (Info.isNetworked)
        {
            NetworkInstanceId id = gameObject.GetComponent<NetworkIdentity>().netId;
            clientObject.GetComponent<PlayerNetwork>().CmdUpdateHealth(id, adj);
        }
    }

    

    void OnDestroy() {
        Destroy(slider);
    }
 

}