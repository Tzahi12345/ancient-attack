﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings {
	public static bool friendlyFire = false;
	public static int maxEngage = -1;
	public static bool assistMode = true;
	public static float volume = 0.5f;
	public static GameObject audioPersist;

	static Settings() {
		Start();
	}

	public static void updateVolume(float newVolume) {
		PlayerPrefs.SetFloat("volume",newVolume);
		volume = newVolume;
		audioPersist.GetComponent<AudioSource>().volume = volume;
		//Debug.Log("New volume: " + volume.ToString());
	}

	// Use this for initialization
	public static void Start() {
		if (PlayerPrefs.HasKey("volume"))
		{
			volume = PlayerPrefs.GetFloat("volume");
		}
		audioPersist = GameObject.Find("AudioPersist");
		audioPersist.GetComponent<AudioSource>().volume = volume;
	}
	
	// Update is called once per frame
}
