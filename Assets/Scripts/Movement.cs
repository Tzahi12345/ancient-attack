﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float speedFactor = 1;
	public bool isMoving;
	public Vector2 target;
	public Vector3 target3;
	[Tooltip("Buffer distance determining how far away the unit stops before reaching its target")]
	public float bufferDistance = 0.1f;
	// Use this for initialization
	void Start () {
		isMoving = false;
		GameObject cameraObj = GameObject.FindWithTag("MainCamera");
		SelectScript selectScript = cameraObj.GetComponent<SelectScript>();
		if (transform.tag == "select")
		{
			selectScript.Deselect();
		}
	}
	
	// Update is called once per frame
	void Update () {
		Unit unitScript = GetComponent<Unit>();

		if (isMoving && !unitScript.isEngaging)
		{
			Vector2 dir = target3 - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			Quaternion qAngle = Quaternion.AngleAxis(angle, Vector3.forward);
			GetComponent<Rigidbody2D>().MoveRotation(angle + Time.deltaTime * 1);
			transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 5 * Time.deltaTime * speedFactor);
			if (Vector2.Distance (transform.position, target) < bufferDistance) { // if it reaches its position, it stops moving
				isMoving = false;
			}
		}
		else if (isMoving && unitScript.isEngaging)
		{
			GameObject targetObject = unitScript.engagedTo;
			Vector2 dir = targetObject.transform.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			GetComponent<Rigidbody2D>().MoveRotation(angle + Time.deltaTime * 1);
			//transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			Vector2 target = new Vector2(targetObject.transform.position.x,
			                             targetObject.transform.position.y);
			bool tempStop = Vector2.Distance (transform.position, target) < bufferDistance;
			if (!tempStop) { // if it reaches its position, it stops moving	
				transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 5 * Time.deltaTime * speedFactor);
			}
		}
		else if (unitScript.isRanged && unitScript.isEngaging)
		{
			Vector2 dir = unitScript.engagedTo.transform.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			GetComponent<Rigidbody2D>().MoveRotation(angle + Time.deltaTime * 1);
			//transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			// Runs for ranged units
		}
	}

	void OnCollisionEnter2D (Collision2D col)
    {
		isMoving = false;
		if (col.gameObject.transform.tag == "enemy")
		{
			Debug.Log("Collided with enemy!");
		}
		else
		{
			Debug.Log("Collided with friendly!");
			
		}
	}

	void onMouseDown() {
		
	}

	void disengage()
	{
		
	}
}
