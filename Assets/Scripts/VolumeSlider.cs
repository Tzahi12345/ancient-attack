﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour {

	public Slider volumeController;
	GameObject camera;

	// Use this for initialization
	void Start () {
		camera = GameObject.FindGameObjectWithTag("MainCamera");
		//Debug.Log("volume is " + Settings.volume.ToString());
		volumeController.value = Settings.volume;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ValueChangeCheck() {
		Settings.updateVolume(volumeController.value);
	}
}
