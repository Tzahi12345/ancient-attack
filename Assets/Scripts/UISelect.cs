﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISelect : MonoBehaviour {
	public Button startStopButton;
	public bool AIEnabled;
	bool selectMode;
	string unitType;
	Vector2 origin;
	Vector3 origin3;
	SelectScript selectScript;
	Text startStopButtonText;
	GameObject selectedObj;
	Image image;
	Color oldColor;
	float timer;
	// Use this for initialization
	void Start () {
		AIEnabled = false;
		selectMode = false;
		selectScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SelectScript>();
		Button startStopBtn = startStopButton.GetComponent<Button>();
		startStopBtn.onClick.AddListener(toggleAI);
		startStopButtonText = startStopButton.GetComponentInChildren<Text>();
		timer = 0;
	}

	void enableAI() {
		AIEnabled = true;
		List<GameObject> enemies = Unit.allEnemies;
		for (int i = 0; i < enemies.Count; i++)
		{
			enemies[i].GetComponent<AIMovement>().enabled = true;
		}
		startStopButtonText.text = "Stop";
	}

	void disableAI() {
		AIEnabled = false;
		List<GameObject> enemies = Unit.allEnemies;
		for (int i = 0; i < enemies.Count; i++)
		{
			enemies[i].GetComponent<AIMovement>().enabled = false;
		}
		startStopButtonText.text = "Start";
	}

	void toggleAI() {
		if (AIEnabled)
		disableAI();
		else
		enableAI();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer > 1f)
		{
			timer = 0;
			if (AIEnabled)
				enableAI();
			else
				disableAI();
			
		}
		if (Input.GetMouseButtonDown(0))
		{
			origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
			                             Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
			PointerEventData pointer = new PointerEventData(EventSystem.current);
             pointer.position = Input.mousePosition;
 
             List<RaycastResult> raycastResults = new List<RaycastResult>();
             EventSystem.current.RaycastAll(pointer, raycastResults);
 
			if (selectMode == false) 
			{
				if (raycastResults.Count > 0)
				{
					GameObject hitObject = raycastResults[0].gameObject;
					string hitTag = hitObject.tag;;
					if (hitTag == "add")
					{
						
						if (selectScript.selectedExists)
						{
							selectScript.Deselect();
						}
						string objName = hitObject.transform.name;
						unitType = objName.Substring(0,objName.Length-2);
						selectedObj = hitObject;
						selectUI(unitType); // select object with name - 2 to remove 'ui'
						Debug.Log(unitType);
					}
				}
			}
			else 
			{
				if (raycastResults.Count > 0)
				{
					GameObject hitObject = raycastResults[0].gameObject;
					if (hitObject == selectedObj)
					{
						deselectUI();
					}
					else
					{
						deselectUI();
						string hitTag = hitObject.tag;
						Debug.Log(hitTag);
						if (hitTag == "add")
						{
							
							if (selectScript.selectedExists)
							{
								selectScript.Deselect();
							}
							string objName = hitObject.transform.name;
							unitType = objName.Substring(0,objName.Length-2);
							selectedObj = hitObject;
							selectUI(unitType); // select object with name - 2 to remove 'ui'
							Debug.Log(unitType);
						}
					}
				}
				else
				{
					Instantiate(Resources.Load<GameObject>(unitType),origin,Quaternion.identity);
					deselectUI();
				}
			}

		}
	}

	void selectUI(string type) {
		selectMode = true;
		unitType = type;
		image = selectedObj.GetComponent<Image>();
		oldColor = image.color;
		image.color = new Color(0f, 1f, 0f, 1f);
	}

	void deselectUI() {
		selectMode = false;
		image.color = oldColor;
	}
}
