﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayNewClip : MonoBehaviour {
	public AudioClip newClip;
	GameObject audioPersist;
	// Use this for initialization
	void Start () {
		audioPersist = GameObject.Find("AudioPersist");
		AudioSource audio = audioPersist.GetComponent<AudioSource>();
		audio.clip = newClip;
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
