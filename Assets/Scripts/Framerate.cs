﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Framerate : MonoBehaviour {
	public bool fpsEnabled;
	public bool showAverage;
	int m_frameCounter = 0;
	float m_timeCounter = 0.0f;
	float m_lastFramerate = 0.0f;
	public float m_refreshTime = 0.5f;
	public GameObject Indicator;
	Text indicatorText;
	List<float> framerates;
	// Use this for initialization
	void Start () {
		indicatorText = Indicator.GetComponent<Text>();
		framerates = new List<float>();
	}
	
	// Update is called once per frame
	void Update () {
		if (fpsEnabled)
		{
			string fullText;
			if (m_timeCounter < m_refreshTime)
			{
				m_timeCounter += Time.deltaTime;
				m_frameCounter++;
			}
			else
			{
				//This code will break if you set your m_refreshTime to 0, which makes no sense.
				m_lastFramerate = (float)m_frameCounter/m_timeCounter;
				m_frameCounter = 0;
				m_timeCounter = 0.0f;
			}

			
			fullText = m_lastFramerate.ToString();
			if (showAverage)
			{
				framerates.Add(m_lastFramerate);
				float average = framerates.Sum()/framerates.Count;
				fullText += "\n" + average.ToString();
			}
			indicatorText.text = fullText;
		}
	}
}
