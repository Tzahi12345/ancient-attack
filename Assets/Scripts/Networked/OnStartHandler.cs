﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnStartHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Info.isNetworked = true;
		InfoMulti.isNetworked = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
