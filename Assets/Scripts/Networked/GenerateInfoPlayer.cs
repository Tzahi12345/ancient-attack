﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GenerateInfoPlayer : MonoBehaviour {

	bool pastState;

	// Use this for initialization
	void Start () {
		Init();
	}
	
	// Update is called once per frame
	void Update () {
		if (pastState != (GetComponent<NetworkIdentity>().isServer))
		{
			Debug.Log("isServer has changed!");
			Init();
		}
		else
		{

		}
	}

	void Init() {
		if (GetComponent<NetworkIdentity>().isServer)
		{
			InfoMulti.runWhenServer();
			pastState = true;
		}
		else
		{
			InfoMulti.runWhenNotServer();
			pastState = false;
		}
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<StartStopHandler>().updateText();
	}
}
