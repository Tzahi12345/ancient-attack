﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public static class InfoMulti {

	private static List<GameObject> enemies = new List<GameObject>();
	public static int teamID;
	public static bool isNetworked = true;

	public static GameObject clientObject;
	public static bool isServer;

	public static void runWhenServer() {
		isServer = true;
	}

	public static void runWhenNotServer() {
		isServer = false;
	}

	static InfoMulti() {

	}

	static List<GameObject> toList(GameObject[] inputObjects) {
		List<GameObject> objectsTemp = new List<GameObject>(); // instantiates list of combatants
		for (int i = 0; i < inputObjects.Length; i++) // Converts the array into a list
		{
			objectsTemp.Add(inputObjects[i]);
		}
		return objectsTemp;
	}

	public static List<GameObject> getEnemies() {
		GameObject[] comradesArr = GameObject.FindGameObjectsWithTag("regular");
		enemies = toList(comradesArr); // gets list of comrades
		List<GameObject> realEnemies = new List<GameObject>();
		for (int i = 0; i < enemies.Count; i++)
		{
			if (enemies[i].GetComponent<NetworkIdentity>().hasAuthority == false)
			{
				realEnemies.Add(enemies[i]);
			}
		}
		return realEnemies;
	}

	public static List<GameObject> getEngagedUnits() { // returns list of engaged units
		List<GameObject> engagedEnemies = new List<GameObject>();
		List<GameObject> currentUnits = getEnemies(); // right now only gets units that the enemy is engaged to
		Unit tempScript;
		foreach (GameObject unit in currentUnits)
		{
			tempScript = unit.GetComponent<Unit>();
			engagedEnemies.Add(tempScript.engagedTo);
		}
		return engagedEnemies;
	}

	public static List<GameObject> getFriendlies() {
		GameObject[] friendliesArrTemp = GameObject.FindGameObjectsWithTag("regular");
		GameObject addon = GameObject.FindGameObjectWithTag("select");
		List<GameObject> friendliesTemp = toList(friendliesArrTemp); // instantiates list of friendlies

		if (addon != null)
		{
			friendliesTemp.Add(addon);
		}
		List<GameObject> realFriendlies = new List<GameObject>();
		for (int i = 0; i < friendliesTemp.Count; i++)
		{
			if (friendliesTemp[i].GetComponent<NetworkIdentity>().hasAuthority == true)
			{
				realFriendlies.Add(friendliesTemp[i]);
			}
		}
		return realFriendlies;
	}
}
