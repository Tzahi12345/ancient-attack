﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using cakeslice;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SelectScriptMulti : NetworkBehaviour {
	public Vector2 origin;
	public Vector3 origin3;
	public GameObject selectObj;
	public Transform select;
	string regularTag = "regular";
	string selectedTag = "select";
	public bool selectedExists;
	public Unit hitUnit;
	bool isOwn;
	public bool AIEnabled;
	bool selectMode;
	string unitType;
	SelectScriptMulti selectScript;
	Text startStopButtonText;
	GameObject selectedObj;
	Image image;
	Color oldColor;
	float timer;
	public GameObject spawnedObj;
	int prevEnemyCount;
	public bool unitSelectionEnabled;
	public bool UISelectionEnabled;

	public void Deselect() {
		//stopUnit();
		Transform select = GameObject.FindWithTag("select").transform;
		SpriteRenderer renderer = GameObject.FindWithTag("select").GetComponent<SpriteRenderer>();
		renderer.color = new Color(1f, 1f, 1f, 1f);
		select.tag = regularTag;
		selectedExists = false;
		// makes sure outline is disabled for all enemy units
		List<GameObject> enemies = InfoMulti.getEnemies();
		for (int i = 0; i < enemies.Count; i++)
		{
			disableOutline(enemies[i]);
		}
		Debug.Log("Deselected on selectscript");
	}

	public void enableAddingOnly() {
		unitSelectionEnabled = false;
		UISelectionEnabled = true;
	}

	public void enableSelectOnly() {
		unitSelectionEnabled = true;
		UISelectionEnabled = false;
	}

	void Select(RaycastHit2D hit) {
		GameObject obj = hit.transform.gameObject;
		if (isAssisted(obj))
		{
			Unit unitScript = obj.GetComponent<Unit>();
			enableOutline(unitScript.engagedTo);
		}
		hit.transform.gameObject.tag = "select";
		SpriteRenderer renderer = GameObject.FindWithTag("select").GetComponent<SpriteRenderer>();
		renderer.color = new Color(0f, 1f, 0f, 1f);
	}

	void moveUnit() {
		Debug.Log("Uh oh first");
		select = GameObject.FindWithTag("select").transform;
		selectObj = GameObject.FindWithTag("select");
		Vector2 aPosition1 = origin;
		//Debug.Log (origin.ToString ());
		MovementMulti moveScript = selectObj.GetComponent<MovementMulti>();
		if (isAssisted(selectObj))
		{
			Unit unitScript = selectObj.GetComponent<Unit>();
			disableOutline(unitScript.engagedTo);
			unitScript.disengageUnit();
		}
		
		moveScript.isMoving = true;
		moveScript.target = origin;
		moveScript.target3 = origin3;
	}

	public void disableOutline(GameObject obj)
	{
		if (obj != null)
		{
			cakeslice.Outline outline = obj.GetComponent<cakeslice.Outline>();
			outline.enabled = false;
		}
	}

	public void enableOutline(GameObject obj)
	{
		if (obj != null)
		{
			cakeslice.Outline outline = obj.GetComponent<cakeslice.Outline>();
			outline.enabled = true;
		}
	}

	void moveGuided(GameObject enemy) {
		selectObj = GameObject.FindWithTag("select");
		select = selectObj.transform;
		Unit unitScript = selectObj.GetComponent<Unit>();
		if (isAssisted(selectObj)) // disables old outline if exists
		{
			disableOutline(unitScript.engagedTo);
		}
		unitScript.engageUnit(enemy);
		enableOutline(enemy);
		MovementMulti moveScript = selectObj.GetComponent<MovementMulti>();
		moveScript.isMoving = true;
	}

	void rangeAttack(GameObject enemy) {
		selectObj = GameObject.FindWithTag("select");
		select = selectObj.transform;	
		selectObj.GetComponent<Unit>().engageUnit(enemy);
		enableOutline(enemy);
		MovementMulti moveScript = selectObj.GetComponent<MovementMulti>();
		moveScript.isMoving = false;
	}

	bool isAssisted(GameObject obj) {
		Unit unitScript = obj.GetComponent<Unit>();
		if (unitScript.isEngaging)
		return true;
		else
		return false;
	}

	public void stopUnit() {
		selectObj = GameObject.FindWithTag("select");
		MovementMulti moveScript = selectObj.GetComponent<MovementMulti>();
		moveScript.isMoving = false;
	}

	// Use this for initialization
	void Start () {
		AIEnabled = false;
		selectMode = false;
		selectScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SelectScriptMulti>();
		timer = 0;
		enableAddingOnly();
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer > 0.2f)
		{
			List<GameObject> tempEnemies = InfoMulti.getEnemies();
			if (prevEnemyCount != tempEnemies.Count)
			{
				for (int i = 0; i < tempEnemies.Count; i++)
				{
					SpriteRenderer renderer = tempEnemies[i].GetComponent<SpriteRenderer>();
					if (renderer.color != new Color(1f, 0f, 0f, 1f))
					renderer.color = new Color(1f, 0f, 0f, 1f);
				}
			}
			prevEnemyCount = tempEnemies.Count;
			timer = 0;
		}

		selectedExists = (GameObject.FindWithTag("select") != null);
		if (Input.GetMouseButtonDown(0))
		{
			if (selectedExists) {
			}

			var layerMask = 1 << 2;

			layerMask = ~layerMask;

			origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
			                             Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
			origin3 = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
			                      Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
			                      Camera.main.ScreenToWorldPoint(Input.mousePosition).z);
			RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero, 0f, layerMask);
			PointerEventData pointer = new PointerEventData(EventSystem.current);
			pointer.position = Input.mousePosition;

			List<RaycastResult> raycastResults = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointer, raycastResults);
			if (UISelectionEnabled)
			{
				if (selectMode == false) 
				{
					if (raycastResults.Count > 0)
					{
						GameObject hitObject = raycastResults[0].gameObject;
						string hitTag = hitObject.tag;
						Debug.Log(hitTag);
						if (hitTag == "add")
						{
							if (selectScript.selectedExists)
							{
								Debug.Log("Should deselect");
								selectScript.Deselect();
								Debug.Log("Deselected on UI");
							}
							string objName = hitObject.transform.name;
							unitType = objName.Substring(0,objName.Length-2);
							selectedObj = hitObject;
							selectUI(unitType); // select object with name - 2 to remove 'ui'
							Debug.Log(unitType);
						}
					}
				}
				else 
				{
					if (raycastResults.Count > 0)
					{
						GameObject hitObject = raycastResults[0].gameObject;
						if (hitObject == selectedObj)
						{
							deselectUI();
						}
						else
						{
							deselectUI();
							string hitTag = hitObject.tag;
							Debug.Log(hitTag);
							if (hitTag == "add")
							{
								if (selectScript.selectedExists)
								{
									selectScript.Deselect();
								}
								string objName = hitObject.transform.name;
								unitType = objName.Substring(0,objName.Length-2);
								selectedObj = hitObject;
								selectUI(unitType); // select object with name - 2 to remove 'ui'
								Debug.Log(unitType);
							}
						}
					}
					else
					{
						GameObject clientObject;
						GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
						for (int i = 0; i < objects.Length; i++)
						{
							GameObject cur = objects[i];
							if (cur.GetComponent<NetworkIdentity>().isLocalPlayer)
							{
								clientObject = cur;
								clientObject.GetComponent<PlayerNetwork>().CmdSetAuthority(origin, unitType);
							}
						}
						deselectUI();
					}
				}
			}

			/** PREVIOUS ONLY BELOW WAS IN SELECTSCRIPTMULTI */
			if (unitSelectionEnabled)
			{
				if (hit)
				{
					isOwn = hit.transform.gameObject.GetComponent<NetworkIdentity>().hasAuthority;
				}
				if (hit && isOwn) { //if it hits and the hit is not an enemy or if its a tag
					string hitTag = hit.transform.gameObject.tag;
					if (selectedExists)
					{
						Deselect();
						if (hitTag == regularTag)
						{
							Select(hit);
						}
					}
					else
					{
						if (hitTag == regularTag)
						{
							Select(hit);
						}
					}
				}
				else if (hit && isOwn == false && Settings.assistMode)
				{
					if (selectedExists)
					{
						selectObj = GameObject.FindWithTag("select");
						select = selectObj.transform;
						if (selectObj.GetComponent<Unit>().isRanged == false)
						{
							moveGuided(hit.transform.gameObject);
						}
						else
						{
							if (Vector2.Distance(select.position, hit.transform.position) <= selectObj.GetComponent<RangedAttackMulti>().rangeDistance)
							{
								rangeAttack(hit.transform.gameObject);
							}
						}
					}
				}
				else
				{
					if (selectedExists)
					{
						moveUnit();
					}
				}
			}
		}
	}

	/** METHODS FROM UISELECTMULTI */

	void selectUI(string type) {
		selectMode = true;
		unitType = type;
		image = selectedObj.GetComponent<Image>();
		oldColor = image.color;
		image.color = new Color(0f, 1f, 0f, 1f);
	}

	void deselectUI() {
		selectMode = false;
		image.color = oldColor;
	}
}
