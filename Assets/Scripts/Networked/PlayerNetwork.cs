﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerNetwork : NetworkBehaviour {
	public GameObject unit;

	// Use this for initialization
	void Start () {
	}

	[Command]
	public void CmdSetAuthority(Vector2 origin, string type) {
		//Instantiate(Resources.Load<GameObject>("Network/"+unitType),origin,Quaternion.identity);
		unit = (GameObject)Instantiate(Resources.Load<GameObject>("Network/"+type), origin, Quaternion.identity);
		//Debug.Log("Done!");
		NetworkServer.SpawnWithClientAuthority(unit, connectionToClient);
		//RpcChangeTag("tempAdded");
	}

	[Command]
	public void CmdSimulateProjectile(NetworkInstanceId id, NetworkInstanceId engagedToID, Vector3 pos, string projectile, Quaternion orientation, Vector3 direction, float time) {
		GameObject obj = ClientScene.FindLocalObject(id);
		GameObject engagedToObj = ClientScene.FindLocalObject(engagedToID);
		
		GameObject arrow = GameObject.Instantiate(Resources.Load<GameObject>("Projectiles/"+projectile),pos,transform.rotation);
		Rigidbody2D rigidbody = arrow.GetComponent<Rigidbody2D>();

		direction = Quaternion.AngleAxis(-45, Vector3.forward) * direction;
		rigidbody.velocity = direction * 50;

		arrow.transform.rotation = orientation;

		Destroy(arrow, time);
	}

	[Command]
    public void CmdUpdateHealth(NetworkInstanceId id,int amount) {
		GameObject obj = ClientScene.FindLocalObject(id);
        obj.GetComponent<PlayerHealth>().curHealth += amount;
    }

	[Command]
	public void CmdUpdateRotation(NetworkInstanceId id, float angle) {
		GameObject obj = ClientScene.FindLocalObject(id);
		obj.GetComponent<Rigidbody2D>().MoveRotation(angle + Time.deltaTime * 1);
		obj.transform.rotation =  Quaternion.AngleAxis(angle, Vector3.forward);
	}

	[ClientRpc]
	void RpcChangeTag(string tag)
    {
        unit.transform.tag = tag;
    }

	[ClientRpc]
	public void RpcSimulateProjectile(NetworkInstanceId id, NetworkInstanceId engagedToID, Vector3 pos, string projectile, Quaternion orientation, Vector3 direction, float time) {
		if (InfoMulti.isServer == false)
		{
			GameObject obj = ClientScene.FindLocalObject(id);
			GameObject engagedToObj = ClientScene.FindLocalObject(engagedToID);
			
			GameObject arrow = GameObject.Instantiate(Resources.Load<GameObject>("Projectiles/"+projectile),pos,transform.rotation);
			Rigidbody2D rigidbody = arrow.GetComponent<Rigidbody2D>();

			direction = Quaternion.AngleAxis(-45, Vector3.forward) * direction;
			rigidbody.velocity = direction * 50;

			arrow.transform.rotation = orientation;

			Destroy(arrow, time);
		}
	}

	[ClientRpc]
	public void RpcEnableSelectOnlyMode() {
		GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
		SelectScriptMulti selectScript = camera.GetComponent<SelectScriptMulti>();
		selectScript.enableSelectOnly();
		NotificationHandler notification = camera.GetComponent<NotificationHandler>();
		notification.displayText("Attack!", 2f);
		GameObject.Find("StartStopButton").SetActive(false);
	}

	[ClientRpc]
	public void RpcUpdateRotations(NetworkInstanceId[] ids, Quaternion[] rotations)
	{
		for (int i = 0; i < ids.Length; i++)
		{
			NetworkInstanceId id = ids[i];
			GameObject obj = ClientScene.FindLocalObject(id);
			obj.transform.rotation = rotations[i];
		}
	}

	/*
	[ClientRpc]
	public void RpcUpdateRotation(NetworkInstanceId id, float angle)
	{
		GameObject obj = ClientScene.FindLocalObject(id);
		obj.GetComponent<Rigidbody2D>().MoveRotation(angle + Time.deltaTime * 1);
		obj.transform.rotation =  Quaternion.AngleAxis(angle, Vector3.forward);
	}*/
	
	// Update is called once per frame
	void Update () {
		
	}
}
