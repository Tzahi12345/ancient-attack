﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class MovementMulti : NetworkBehaviour {

	public float speedFactor = 1;
	public bool isMoving;
	public Vector2 target;
	public Vector3 target3;
	Rigidbody2D rigidBody;
	// Use this for initialization
	void Start () {
		isMoving = false;
		GameObject cameraObj = GameObject.FindWithTag("MainCamera");
		SelectScriptMulti selectScript = cameraObj.GetComponent<SelectScriptMulti>();
		if (transform.tag == "select")
		{
			selectScript.Deselect();
		}
		rigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		//resetVelocity();
		Unit unitScript = GetComponent<Unit>();

		if (isMoving && !unitScript.isEngaging)
		{
			Vector2 dir = target3 - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			//rigidBody.MoveRotation(angle + Time.deltaTime * 1);
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 5 * Time.deltaTime * speedFactor);
			if (Vector2.Distance (transform.position, target) < 10f) { // if it reaches its position, it stops moving
				isMoving = false;
			}
		}
		else if (isMoving && unitScript.isEngaging)
		{
			GameObject targetObject = unitScript.engagedTo;
			Vector2 dir = targetObject.transform.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			//rigidBody.MoveRotation(angle + Time.deltaTime * 1);
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			Vector2 target = new Vector2(targetObject.transform.position.x,
			                             targetObject.transform.position.y);
			bool tempStop = Vector2.Distance (transform.position, target) < 10f;
			if (!tempStop) { // if it reaches its position, it stops moving	
				transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), target, 5 * Time.deltaTime * speedFactor);
			}
		}
		else if (unitScript.isRanged && unitScript.isEngaging)
		{
			Vector2 dir = unitScript.engagedTo.transform.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90f;
			//rigidBody.MoveRotation(angle + Time.deltaTime * 1);
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			// Runs for ranged units
		}
	}

	void OnCollisionEnter2D (Collision2D col)
    {
		if (gameObject.GetComponent<NetworkIdentity>().hasAuthority && col.gameObject.GetComponent<NetworkIdentity>().hasAuthority == false)
		{
			Debug.Log("Collided with enemy!");
			isMoving = false;
		}
		else
		{
			Debug.Log("Collided with friendly!");
			
		}
	}

	void OnCollisionExit2D() {
		resetVelocity();
	}

	void onMouseDown() {
		
	}

	void disengage()
	{
		
	}

	void resetVelocity() {
		rigidBody.velocity = Vector2.zero;
		rigidBody.angularVelocity = 0f;
	}
}
