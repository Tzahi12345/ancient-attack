# README #

Thanks for visiting my project's git page, read below to get some information on what Ancient Attack is and how you can contribute.

### What is Ancient Attack? ###

Summary

* Ancient Attack is a 2D strategy game built on Unity, where you command an army from the perspective of an ancient general. You can move units around, strategically place them around the map, and attack and crush the enemy.

Version

* There are no version numbers yet, the project is currently in pre-alpha. When some key features get added (such as multiplayer support), this will change.

### How do I get set up? ###

It's Easy

* Simply download Unity 5, clone this git repository, and you should be good to go. The scripts are all located in the Assets folder, and prefabs are in the Resources folder within Assets. The folder called Imports are where all the dependencies are.

* Building the project isn't difficult either, and it should work on most Desktop platforms without issue.

Want to try it out?

* [Click here!](https://isaac.grynsztein.com) It's an older build and not always up-to-date, but it'll give you an idea on what this project is about.

### Contribution guidelines ###

Syntax

* Simply look at the other code and you'll get a general idea of what the syntax should look like. Tabs > spaces. I'm still in the process of cleaning and commenting the code, so don't mind the mess in the meantime.

### Who do I talk to? ###

Me!

* Contact me at tzahi2g@gmail.com and I'll try to respond as soon as possible. If you find a bug, you can contact me or report it through BitBucket.

### Credits ###

* Icons from Icons8: [Arrow Icon](https://icons8.com/icon/35061/Archers-Arrow)
* Battle Music from [Bensound](http://www.bensound.com/)